#if !defined(AFX_ACTDLG_H__E631FEE6_BBF3_4128_AAC1_4777A8F9A5A2__INCLUDED_)
#define AFX_ACTDLG_H__E631FEE6_BBF3_4128_AAC1_4777A8F9A5A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ActDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CActDlg dialog
#include "scene.h"

class CActDlg : public CDialog
{
// Construction
public:
	int m_nType;
	ACT* m_pAct;
	CActDlg(CWnd* pParent = NULL);   // standard constructor
private:
	void UpdatePrompt();
	BOOL CheckValue();
// Dialog Data
	//{{AFX_DATA(CActDlg)
	enum { IDD = IDD_ACTDLG };
	CEdit	m_editPara6;
	CEdit	m_editPara5;
	CEdit	m_editPara4;
	CEdit	m_editPara3;
	CEdit	m_editPara2;
	CEdit	m_editPara1;
	CComboBox	m_comboCommand;
	int		m_nIndex;
	int		m_nPara1;
	int		m_nPara2;
	int		m_nPara3;
	int		m_nPara4;
	CString	m_strPara1;
	CString	m_strPara2;
	CString	m_strPara3;
	CString	m_strPara4;
	int		m_nPara5;
	int		m_nPara6;
	CString	m_strPara5;
	CString	m_strPara6;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CActDlg)
	public:
	virtual int DoModal(int nType,ACT* pAct);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CActDlg)
	afx_msg void OnSelchangeCombo();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACTDLG_H__E631FEE6_BBF3_4128_AAC1_4777A8F9A5A2__INCLUDED_)
