//scene.cpp

#include "stdafx.h"


//
CScene::CScene()
{	//防止销毁的时候出错
	pAct = new ACT[1];
	pClu = new CLU[1];
};
//
CScene::~CScene()
{
	delete [] pAct;
	delete [] pClu;
};
//
BOOL CScene::Open(char* FileName)
{
	HANDLE hFile;
	hFile = CreateFile(FileName,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
	if(hFile==INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}
	DWORD dwActualSize = 0;
	BOOL bReadSt;
	//读出游戏进程ID
	bReadSt = ReadFile(hFile,&nID,sizeof(int),&dwActualSize,NULL);
	if( !bReadSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//读出场类型
	bReadSt = ReadFile(hFile,&nType,sizeof(int),&dwActualSize,NULL);
	if( !bReadSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//读出动作指令的数目
	bReadSt = ReadFile(hFile,&nActNum,sizeof(int),&dwActualSize,NULL);
	if( !bReadSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//
	if( nActNum != 0 )
	{
		//根据动作指令的数目创建动作指令数组
		delete [] pAct;
		pAct = new ACT[nActNum];
		//读出动作指令数组
		bReadSt = ReadFile(hFile,pAct,(sizeof(ACT)*nActNum),&dwActualSize,NULL);
		if( !bReadSt )
		{
			CloseHandle(hFile);
			return FALSE;
		}
	}
	//读出计算指令的数目
	bReadSt = ReadFile(hFile,&nCluNum,sizeof(int),&dwActualSize,NULL);
	if( !bReadSt )
	{
		CloseHandle(hFile);
		return FALSE;
	}
	//
	if( nCluNum != 0 )
	{
		//根据计算指令的数目创建动作指令数组
		delete [] pClu;
		pClu = new CLU[nCluNum];
		//读出计算指令数组
		bReadSt = ReadFile(hFile,pClu,(sizeof(CLU)*nCluNum),&dwActualSize,NULL);
		if( !bReadSt )
		{
			CloseHandle(hFile);
			return FALSE;
		}
	}
	//
	CloseHandle(hFile);
	//执行初始化的操作
	nCurAct = 0;
	nCurClu = 0;
	return TRUE;
};
//
BOOL CScene::GetNextAct(ACT* pA)
{
	if( nCurAct < nActNum )
	{
		int n = nCurAct;
		nCurAct ++;
		memcpy( pA, &pAct[n], sizeof(ACT) );
		return TRUE;
	}
	else
	{
		return FALSE;
	}
};
//
BOOL CScene::CluAll(CGameData* pDB)
{
	while( nCurClu < nCluNum )
	{
		switch( pClu[nCurClu].nCluType )
		{
		case 1:
			{
				if( !pDB->CPY(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2) )
				{
					return FALSE;
				}
				break;
			}
		case 2:
			{
				if( !pDB->SET(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2) )
				{
					return FALSE;
				}
				break;
			}
		case 3:
			{
				if( !pDB->ADD(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2,pClu[nCurClu].nPara3) )
				{
					return FALSE;
				}
				break;
			}
		case 4:
			{
				if( !pDB->SUB(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2,pClu[nCurClu].nPara3) )
				{
					return FALSE;
				}
				break;
			}
		case 5:
			{
				if( !pDB->MUL(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2,pClu[nCurClu].nPara3) )
				{
					return FALSE;
				}
				break;
			}
		case 6:
			{
				if( !pDB->DIV(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2,pClu[nCurClu].nPara3) )
				{
					return FALSE;
				}
				break;
			}
		case 7:
			{
				if( !pDB->MOD(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2,pClu[nCurClu].nPara3) )
				{
					return FALSE;
				}
				break;
			}
		case 8:
			{
				if( !pDB->IFB(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2,pClu[nCurClu].nPara3,pClu[nCurClu].nPara4) )
				{
					return FALSE;
				}
				break;
			}
		case 9:
			{
				if( !pDB->IFS(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2,pClu[nCurClu].nPara3,pClu[nCurClu].nPara4) )
				{
					return FALSE;
				}
				break;
			}
		case 10:
			{
				if( !pDB->IFE(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2,pClu[nCurClu].nPara3,pClu[nCurClu].nPara4) )
				{
					return FALSE;
				}
				break;
			}
		case 11:
			{
				if( !pDB->AND(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2,pClu[nCurClu].nPara3) )
				{
					return FALSE;
				}
				break;
			}
		case 12:
			{
				if( !pDB->ORL(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2,pClu[nCurClu].nPara3) )
				{
					return FALSE;
				}
				break;
			}
		case 13:
			{
				if( !pDB->NOT(pClu[nCurClu].nPara1,pClu[nCurClu].nPara2) )
				{
					return FALSE;
				}
				break;
			}
		}//switch语句结束
		nCurClu ++;
	}//while语句结束
	return TRUE;
};//CalAll()函数结束
//the end.